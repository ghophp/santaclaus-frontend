# santaclaus-frontend ![build status](https://codeship.com/projects/10c82340-2a9a-0134-7c52-3e78df657623/status?branch=master)

Santa Claus frotend is the client for the [santa-claus-api](https://bitbucket.org/ghophp/santaclaus-api) API. It is basically a SPA that consumes and coordinate the usability of the service.

Setup
-----

Installing Dependencies
-----------------------

```
npm install
bower install
```

Config
------

`cp config.json.dist config.json`

Please notice that the config contains the port for the server side.

Build and Run
-------------

`grunt dist`

The result of the `grunt dist` is a `dist` folder containing a ready to run project. To run the application, the best thing is use a simple application server, you can use a simple `nginx` or `apache`. I particularly execute this from the dist folder:

`php -S localhost:8080`
