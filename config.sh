#!/bin/sh

if test -n "${CI_BRANCH}"; then
	if test "${CI_BRANCH}" = "develop"; then
		cp config.json.staging config.json;
	else
		cp config.json.prod config.json;
	fi;
fi;