app.controller('DashboardController', ['$scope', '$rootScope', '$http', 'ngNotify', '$uibModal', function($scope, $rootScope, $http, ngNotify, $uibModal) {
	var MY_ITEMS = 'my-items';
	var GIFTS = 'gifts';

	$scope.name = function() {
		return $scope.state == MY_ITEMS ? 'items' : 'gifts';
	};

	$scope.logout = function() {
		FB.logout(function(response) {
			document.location.reload();
		});
	};

	$scope.loadMyItems = function() {
		if ($scope.state == MY_ITEMS) {
			return;
		}

		$rootScope.loading = true;

		$scope.items = [];
		$scope.state = MY_ITEMS;
		$scope.title = 'My Items';

		$http({
			method: 'GET',
			url: BASE_URL + 'v1/me/items',
		}).then(function successCallback(response) {
			$rootScope.loading = false;
			$scope.items = response.data;
		}, function errorCallback(response) {
			$rootScope.loading = false;
			ngNotify.set('There was a problem listing your items, please try again!' + ' ['+response.data.message+']');
		});
	};

	$scope.loadGifts = function(force) {
		if ($scope.state == GIFTS && !force) {
			return;
		}

		$rootScope.loading = true;

		$scope.items = [];
		$scope.state = GIFTS;
		$scope.title = 'My Gifts';

		$http({
			method: 'GET',
			url: BASE_URL + 'v1/me/gifts',
		}).then(function successCallback(response) {
			$rootScope.loading = false;
			$scope.items = response.data;
		}, function errorCallback(response) {
			$rootScope.loading = false;
			ngNotify.set('There was a problem listing your gifts, please try again!' + ' ['+response.data.message+']');
		});
	};

	$scope.sendItem = function(item) {
		$rootScope.loading = true;
		FB.api('/' + $rootScope.user.id + '/friends', function(response) {
			var modalInstance = $uibModal.open({
				animation: $scope.animationsEnabled,
				templateUrl: 'app/src/app/controllers/dashboard/send.html',
				controller: 'SendModalController',
				resolve: {
					friends: function() {
						return response.data;
					},
					item: function() {
						return item;
					}
				}
			});

			modalInstance.result.then(function (result) {
				if (result.friend && result.quantity) {
					$http({
						method: 'POST',
						url: BASE_URL + 'v1/me/item/' + item.id + '/send/' + result.friend,
						data: {quantity: result.quantity},
					}).then(function successCallback(response) {
						ngNotify.set('Gift sent with success!');
					}, function errorCallback(response) {
						if (response.status == 409) {
							ngNotify.set('You already sent this gift to this friend :)');	
							return;
						}

						ngNotify.set('There was a problem sending the item, please try again!' + ' ['+response.data.message+']');
					});
				}
			});

			$rootScope.loading = false;
		});
	};

	$scope.claimGift = function(item) {
		$http({
			method: 'POST',
			url: BASE_URL + 'v1/me/gift/' + item.id + '/claim/' + item.sender_id
		}).then(function successCallback(response) {
			ngNotify.set('You claimed your gift!');
			$scope.loadGifts(true);
		}, function errorCallback(response) {
			if (response.status == 409) {
				ngNotify.set('Your friend used the items :) try to claim gifts as soon as possible!');
				return;
			}

			ngNotify.set('There was a problem sending the item, please try again!' + ' ['+response.data.message+']');
		});
	};

	$scope.onItemClick = function(item) {
		if ($scope.state == MY_ITEMS) {
			$scope.sendItem(item);	
		} else {
			$scope.claimGift(item);
		}
	};

	$scope.isMyItemsState = function() {
		return $scope.state == MY_ITEMS;
	};

	$scope.isGiftsState = function() {
		return $scope.state == GIFTS;
	};

	$rootScope.$watch("user", function(){
		if ($rootScope.user) {
			$scope.loadMyItems();
		}
	});
}]);
