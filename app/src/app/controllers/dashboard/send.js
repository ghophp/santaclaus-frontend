app.controller('SendModalController', ['$scope', '$rootScope', '$uibModalInstance', 'ngNotify', 'item', 'friends', function($scope, $rootScope, $uibModalInstance, ngNotify, item, friends) {
	$scope.select = function(friend) {
		$scope.selected = friend.id;
	};

	$scope.ok = function() {
		var quantity = parseInt($('input[name="quantity"]').val(), 10);
		if ($scope.selected <= 0) {
			ngNotify.set("Please select a friend", 'notice');
			return;
		}
		console.log(quantity);
		if (quantity <= 0 || quantity > item.quantity) {
			ngNotify.set("Please inform quantity bigger then zero and less then the number of items you have", 'notice');
			return;
		}

		$uibModalInstance.close({'friend': $scope.selected, 'quantity': quantity});
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.selected = 0;
	$scope.max = item.quantity;
	$scope.friends = friends;
}]);
