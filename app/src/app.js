var app = angular.module('myapp', ['ngCookies', 'ngNotify', 'ngAnimate', 'ui.router', 'ui.bootstrap'])

.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider) {
	$httpProvider.defaults.useXDomain = true;

	$urlRouterProvider.otherwise("/");
	$stateProvider
		.state('index', {
			url: "/",
			templateUrl: "app/src/app/controllers/main.html"
		});
	$stateProvider
		.state('dashboard', {
			url: "/dashboard",
			templateUrl: "app/src/app/controllers/dashboard/dashboard.html"
		});
}])

.run(['$rootScope', '$location', '$cookies', '$http', 'ngNotify', function($rootScope, $location, $cookies, $http, ngNotify) {
	ngNotify.config({
		theme: 'pure',
		position: 'bottom',
		duration: 3000,
		type: 'info',
		sticky: false,
		button: true,
		html: false
	});

	$rootScope.loading = true;

	$rootScope.checkLoginStatus = function() {
		$rootScope.$apply(function(){
			$rootScope.loading = true;	
		});
		
		FB.getLoginStatus(function(response) {
			$rootScope.statusChangeCallback(response);
		});
	};

	$rootScope.statusChangeCallback = function(response) {
		if (response.status === 'connected') {
			var accessToken = response.authResponse.accessToken;
			FB.api('/me', function(response) {
				var user = {
					name: response.name, 
					id: parseInt(response.id, 10)
				};

				$http({
					method: 'POST',
					url: BASE_URL + 'v1/me/register',
					data: user,
				}).then(function successCallback(response) {
					
					$http.defaults.headers.common['Authorization'] = accessToken; // jshint ignore:line
					$rootScope.user = user;
					
					$location.path('/dashboard');
					$rootScope.loading = false;
					
				}, function errorCallback(response) {
					ngNotify.set('There was a problem registering you, please try again!');
				});
			});
		} else {
			$location.path('/');
			$rootScope.loading = false;
		}

		$rootScope.$apply();
	};

	window.fbAsyncInit = function() {
		FB.init({
			appId      : '1742609389358069',
			xfbml      : true,
			version    : 'v2.6'
		});

		$rootScope.checkLoginStatus();
	};
}]);
