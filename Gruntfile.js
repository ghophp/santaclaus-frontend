module.exports = function(grunt) {

  var config = grunt.file.readJSON('config.json');

  grunt.initConfig({

    cfg: config,

    // COPY VENDOR =============================================================

    bowercopy: {
      options: {
        srcPrefix: "bower_components"
      },
      scripts: {
        options: {
          destPrefix: "dist/vendor"
        },
        files: {
          'moment/moment.min.js': 'moment/min/moment.min.js',
          'jquery/jquery.min.js': 'jquery/dist/jquery.min.js',
          'jquery/jquery.min.map': 'jquery/dist/jquery.min.map',
          'angular/angular.min.js': 'angular/angular.min.js',
          'angular/angular.min.js.map': 'angular/angular.min.js.map',
          'angular-bootstrap/ui-bootstrap.min.js': 'angular-bootstrap/ui-bootstrap.min.js',
          'angular-bootstrap/ui-bootstrap-tpls.min.js': 'angular-bootstrap/ui-bootstrap-tpls.min.js',
          'angular-animate/angular-animate.min.js': 'angular-animate/angular-animate.min.js',
          'angular-animate/angular-animate.min.js.map': 'angular-animate/angular-animate.min.js.map',
          'angular-cookies/angular-cookies.min.js': 'angular-cookies/angular-cookies.min.js',
          'angular-cookies/angular-cookies.min.js.map': 'angular-cookies/angular-cookies.min.js.map',
          'angular-ui-router/angular-ui-router.js': 'angular-ui-router/release/angular-ui-router.js',
          'ng-notify/ng-notify.min.js': 'ng-notify/dist/ng-notify.min.js',
          'ng-notify/ng-notify.min.js.map': 'ng-notify/dist/ng-notify.min.js.map',
          'font-awesome/font-awesome.min.css': 'font-awesome/css/font-awesome.min.css',
          'font-awesome/font-awesome.css.map': 'font-awesome/css/font-awesome.css.map',
          'bootstrap/bootstrap-theme.min.css': 'bootstrap/dist/css/bootstrap-theme.min.css',
          'bootstrap/bootstrap-theme.min.css.map': 'bootstrap/dist/css/bootstrap-theme.min.css.map',
        }
      }
    },

    // INDEX =================================================================

    copy: {
      main: {
        files: [
          {expand: true, flatten: true, src: ['app/fonts/*/*'], dest: 'dist/fonts/', filter: 'isFile'},
          {src: ['app/404.html'], dest: 'dist/404.html'},
        ],
      },
      index:  {
        options: {
          processContent: function (content, srcpath) {
            return grunt.template.process(content);
          }
        },
        src:  'app/index.html',
        dest: 'dist/index.html'
      }
    },

    // IMAGES =================================================================

    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'app/images/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'dist/images/'
        }]
      }
    },

    // ANGULAR TEMPLATES =======================================================

    ngtemplates: {
      myapp: {
        src:      'app/src/**/*.html',
        dest:     'dist/template.js'
      }
    },

    // JS TASKS ================================================================
    jshint: {
      all: ['app/src/**/*.js'] 
    },

    uglify: {
      build: {
        options: {
          beautify : true,
          mangle   : true
        },
        files: {
          'dist/app.min.js': ['app/src/**/*.js', 'app/src/*.js', '!app/app.js', 'app/app.js']
        }
      }
    },

    // CSS TASKS ===============================================================
    less: {
      build: {
        files: {
          'dist/style.css': 'app/src/style/style.less'
        }
      }
    },

    cssmin: {
      build: {
        files: {
          'dist/style.min.css': 'dist/style.css'
        }
      }
    },

    // COOL TASKS ==============================================================
    watch: {
      css: {
        files: ['app/src/style/**/*.less', 'app/src/style/**/*.css'],
        tasks: ['less', 'cssmin']
      },
      js: {
        files: ['app/src/**/*.js'],
        tasks: ['jshint', 'uglify']
      },
      html: {
        files: ['app/src/**/*.html'],
        tasks: ['ngtemplates']
      }
    },

    concurrent: {
      options: {
        logConcurrentOutput: true
      },
      tasks: ['watch']
    }   

  });

  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-bowercopy');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('default', ['bowercopy', 'copy', 'ngtemplates', 'less', 'cssmin', 'jshint', 'uglify']);
  grunt.registerTask('dist', ['bowercopy', 'copy', 'imagemin', 'ngtemplates', 'less', 'cssmin', 'jshint', 'uglify']);
};